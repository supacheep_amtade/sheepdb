/*
 * code_generater.hpp
 *
 *  Created on: Feb 16, 2015
 *      Author: ppus
 */

#ifndef CODE_GENERATER_HPP_
#define CODE_GENERATER_HPP_

#include "../utils/db_doer.hpp"

class CodeGenerater : public DBDoer {
public:
	CodeGenerater(){};
	CodeGenerater(char* dblocation) : DBDoer(dblocation){};
	char* execute(char *cmd);
	char* parse_string(char *&str);
	std::string output;
	char* current_addr = "db/";
};

#endif /* CODE_GENERATER_HPP_ */
