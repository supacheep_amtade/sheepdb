/*
 * dblp_parser.hpp
 *
 *  Created on: Feb 13, 2015
 *      Author: ppus
 */

#ifndef DBLP_PARSER_HPP_
#define DBLP_PARSER_HPP_

#include <string>

#include "db_doer.hpp"
#include "counter.hpp"
#include "author_indexer.hpp"

class DBLPParser : public DBDoer{
public:
	DBLPParser(){ initialize("db/dblp");}					/* construct by default */
	DBLPParser(char* dblocation) : DBDoer(dblocation){};	/* construct with db location */
//	~DBLPParser(){}
	bool dojson(const char* data);							/* parse from string */
	bool dofile(char*);										/* parse file */
	char* dbauthor;
private:
	void generate_record(char *);
};

#endif /* DBLP_PARSER_HPP_ */
