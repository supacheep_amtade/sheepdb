/*
 * util_net.hpp
 *
 *  Created on: Feb 16, 2015
 *      Author: ppus
 */

#ifndef UTIL_NET_HPP_
#define UTIL_NET_HPP_

#ifdef __cplusplus
extern "C"
{
#endif

#define RCVBUFSIZE 32   /* Size of receive buffer */
#define PORT_MASTER 26096	/* Port number for sending msg */
#define PORT_WORKER 26097	/* Port number for sending msg */

/* Structure of arguments to pass to client thread */
typedef struct ThreadArgs
{
    int clntSock;		/* Socket descriptor for client */
} ThreadArgs;

void DieWithError(char *errorMessage);  /* Error handling function */
void HandleTCPClient(int clntSocket);   /* TCP client handling function */
int CreateTCPServerSocket(unsigned short port); /* Create TCP server socket */
int AcceptTCPConnection(int servSock);  /* Accept TCP connection request */

#ifdef __cplusplus
}
#endif

#endif /* UTIL_NET_HPP_ */
