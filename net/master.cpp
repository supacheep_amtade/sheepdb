/*
 * master.cpp
 *
 *  Created on: Feb 16, 2015
 *      Author: ppus
 */

#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>

#include "pthread.h"
#include "stdlib.h"
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include "unistd.h"		/* for close() */
#include <sys/socket.h> /* for socket(), bind(), and connect() */

#include "util_net.h"
#include "master.hpp"
#include "../utils/alldef.hpp"
#include "../utils/demon_utils.hpp"

bool debug1 = false;

void EnableDebug(bool d){
	debug1 = d;
}

void* MasterListen(void* arg){
	int servSock;                    /* Socket descriptor for server */
	int clntSock;                    /* Socket descriptor for client */
	unsigned short echoServPort;     /* Server port */
	pthread_t threadID;              /* Thread ID from pthread_create() */
	struct ThreadArgs *threadArgs;   /* Pointer to argument structure for thread */

	echoServPort = PORT_MASTER;

	servSock = CreateTCPServerSocket(echoServPort);

	for (;;) /* run forever */{
		clntSock = AcceptTCPConnection(servSock);

		/* Create separate memory for client argument */
		if ((threadArgs = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs)))
			   == NULL)
			DieWithError("malloc() failed");
		threadArgs -> clntSock = clntSock;

		/* Create client thread */
		if (pthread_create(&threadID, NULL, ThreadMaster, (void *) threadArgs) != 0)
			DieWithError("net: pthread_create() failed");
	}
	/* NOT REACHED */
}

void* MasterSSLListen(void* arg){
	SSL_CTX *ctx;
	int serv;
	int portnum = PORT_SSL;
	pthread_t sslthreadID;              /* Thread ID from pthread_create() */
	struct SSLThreadArgs *sslthreadArgs;   /* Pointer to argument structure for thread */


	ctx = InitServerCTX();		/* initialize SSL */
	LoadCertificates(ctx, ".ssl/sheepdb.crt", ".ssl/sheepdb.key");	/* load certs */
	serv = OpenListener(portnum);				/* create server socket */

	for(;;) /* run forever */{
		struct sockaddr_in addr;
		unsigned int len = sizeof(addr);
		SSL *ssl;

		int client = accept(serv, (struct sockaddr *) &addr, &len);		/* accept connection as usual */
		printf("Connection: %s:%d\n",
			inet_ntoa(addr.sin_addr), ntohs(addr.sin_port)); /* debug connection */
		ssl = SSL_new(ctx);         					/* get new SSL state with context */
		SSL_set_fd(ssl, client);						/* set connection socket to SSL state */

		/* Create separate memory for client argument */
		if ((sslthreadArgs = (struct SSLThreadArgs *) malloc(sizeof(struct SSLThreadArgs)))
			   == NULL)
			DieWithError("malloc() failed");
		sslthreadArgs -> ssl_t = ssl;

		/* Create client thread service connection */
		if (pthread_create(&sslthreadID, NULL, Servlet, (void *) sslthreadArgs) != 0)
			DieWithError("ssl: pthread_create() failed");
	}
	close(serv);										/* close server socket */
	SSL_CTX_free(ctx);									/* release context */
}

void* ThreadMaster(void* arg){
	int clntSock = ((struct ThreadArgs *) arg) -> clntSock;	/* Socket descriptor for client connection */
	free(arg);              /* Deallocate memory for argument */

	MasterRecvResult(clntSock);

//	/* Guarantees that thread resources are deallocated upon return */
//	pthread_detach(pthread_self());

	return (NULL);
}

void MasterRecvResult(int clntSock){
	char echoBuffer[RCVBUFSIZE];        /* Buffer for echo string */
	int recvMsgSize;                    /* Size of received message */

	/* Receive message from client */
	if ((recvMsgSize = recv(clntSock, echoBuffer, RCVBUFSIZE, 0)) < 0)
		DieWithError("recv() failed");

	/* Send received string and receive again until end of transmission */
	while (recvMsgSize > 0){      /* zero indicates end of transmission */
		/* See if there is more data to receive */
		if ((recvMsgSize = recv(clntSock, echoBuffer, RCVBUFSIZE, 0)) < 0)
			DieWithError("recv() failed");
	}

	close(clntSock);    /* Close client socket */
}

void* MasterSend(void* arg){
	struct SentThreadArg *targ = (struct SentThreadArg*) arg;
	int sock;                        /* Socket descriptor */
	struct sockaddr_in workerAddr; /* worker server address */
	unsigned short workerPort = PORT_WORKER;     /* worker server port */
	char* resultBuffer = (char *)malloc(sizeof(char) * RCVBUFSIZE + 1);     /* Buffer for result */
	int recvMsgSize;				/* msg size */
	targ -> reply = (char *)malloc(sizeof(char) * ALLBUF);	/* returned result */
	unsigned int cmdLen;      /* Length of string to echo */
	int bytesRcvd, totalBytesRcvd;   /* Bytes read in single recv()
										and total bytes read */

	/* Create a reliable, stream socket using TCP */
	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		DieWithError("socket() failed");

	/* Construct the server address structure */
	memset(&workerAddr, 0, sizeof(workerAddr));     /* Zero out structure */
	workerAddr.sin_family      = AF_INET;             /* Internet address family */
	workerAddr.sin_addr.s_addr = inet_addr(targ->servIP);   /* Server IP address */
	workerAddr.sin_port        = htons(workerPort); /* Server port */

	/* Establish the connection to the echo server */
	if (connect(sock, (struct sockaddr *) &workerAddr, sizeof(workerAddr)) < 0)
		DieWithError("connect() failed");

	cmdLen = strlen(targ -> cmd);          /* Determine input length */

	/* Send the cmd to the server */
	if (send(sock, targ -> cmd, cmdLen, 0) != cmdLen)
		DieWithError("send() sent a different number of bytes than expected");

	/* Receive the same string back from the server */
	totalBytesRcvd = 0;
	
	/* Receive message from client */
	if ((recvMsgSize = recv(sock, resultBuffer, RCVBUFSIZE, 0)) < 0)
		DieWithError("recv() failed");
	
	resultBuffer[recvMsgSize] = '\0';

	strcat(targ -> reply, resultBuffer);
	totalBytesRcvd += strlen(resultBuffer);

	/* Send received string and receive again until end of transmission */
	while (recvMsgSize == RCVBUFSIZE){      /* zero indicates end of transmission */
		/* See if there is more data to receive */
		if ((recvMsgSize = recv(sock, resultBuffer, RCVBUFSIZE, 0)) < 0)
			if (strcmp(resultBuffer,"ack") != 0  && strcmp(resultBuffer,"nack") != 0)
				DieWithError("recv() failed or connection closed prematurely");

		if (recvMsgSize > 0){
			resultBuffer[recvMsgSize] = '\0';
			strcat(targ -> reply, resultBuffer);
 			totalBytesRcvd += strlen(resultBuffer);
		}
	}

	targ -> reply[totalBytesRcvd+1] = '\0';	/* end the string */

	close(sock);
	
//	printf("%s\n", targ->reply);
//	SSL_write(targ -> ssl, targ -> reply, strlen(targ -> reply));
	pthread_exit(NULL);
}

/* SSL servlet (contexts can be shared) */
/* Serve the connection -- threadable */
void* Servlet(void* arg){
	SSL* ssl = ((struct SSLThreadArgs *) arg)->ssl_t;
	free(arg);
	char *buf = (char *)malloc(sizeof(char) * CMDBUFSIZE);
	int sd, bytes;

	/* pthread init */
//	pthread_t threadID[get_worker_len()];
	struct SentThreadArg *targ[get_worker_len()];
	for(int i = 0 ; i < get_worker_len() ; ++ i ){
		targ[i] = (SentThreadArg *)malloc(sizeof(SentThreadArg));
		targ[i]->cmd = (char *)malloc(sizeof(char)*CMDBUFSIZE);
		targ[i]->servIP = (char *)malloc(sizeof(char)*18);
		targ[i] -> ssl = ssl;
	}
	int rc;

    if ( SSL_accept(ssl) == FAIL )					/* do SSL-protocol accept */
        ERR_print_errors_fp(stderr);
    else{
        ShowCerts(ssl);								/* get any certificates */
        pthread_t threadID[get_worker_len()];
        char *answer;
        while(1){
			bytes = SSL_read(ssl, buf, CMDBUFSIZE);	/* get request */

			if ( bytes > 0 ){
				buf[bytes] = '\0';
				for (int i = 0; i < get_worker_len(); i++){
					if (debug1) printf("Client msg: \"%s\"\n", buf);
					strcpy(targ[i] -> cmd, buf);
					strcpy(targ[i] -> servIP, get_worker(i));
					rc = pthread_create(&threadID[i], NULL, MasterSend, (void *) targ[i]);
					if (rc){
						printf("Error:unable to create thread,%d\n",rc);
					}
				}

				int replyCount = get_worker_len();	/* count available reply */
				for (int i = 0; i < get_worker_len(); i++){
					rc = pthread_join(threadID[i], NULL);
					if (targ[i]->reply && targ[i]->reply[0] == '\0'){
						replyCount--;
					}

					if (debug1 && rc){
						printf("Error:unable to join,%d\n",rc);
					}
					if (debug1){
						printf("thread[%d] => %s\n", i, targ[i] -> reply);
					}
				}

				/* merge reply */
				answer = (char *)malloc(sizeof(char) * replyCount * ALLBUF);
				strcpy(answer, "{\n\0");

				for (int i = 0; i < get_worker_len(); i++){
					if (mergeReply(answer, targ[i]->reply)){
						break;
					}
					free(targ[i]->reply);
				}

				strcat(answer, "}\0");

				if (debug1){
					printf("answer = \n%s\nstring_len=%d\n", answer,strlen(answer));
					printf("job done\n");
				}

				/* send, then free */
				if (SSL_write(ssl, answer, strlen(answer)))
					free(answer);
			}
			else { ERR_print_errors_fp(stderr);}

			if ( strcmp(buf,"exit\n") == 0 ) { break;}
        }
        SSL_write(ssl, "close connection.", strlen("close connection."));	/* send reply */
    }
    sd = SSL_get_fd(ssl);							/* get socket connection */
    SSL_free(ssl);									/* release SSL state */
    close(sd);										/* close connection */

    	/* free heap memory */
	free(buf);
	for(int i = 0 ; i < get_worker_len() ; ++ i ){
		free(targ[i] -> cmd);
		free(targ[i] -> servIP);
		SSL_free(targ[i] -> ssl);
		free(targ[i]);		
	}
	
	pthread_exit(0);
}

int mergeReply(char *&dest, char* source){
	int end = strlen(source);
	if (source[0] == 'd' && source[1] == 'b'){
		strcat(dest, source);
		return 1;
	}
	else{
		source[end - 1] = '\0';
		source = source + 2;
		strcat(dest, source);
	}
	return 0;
}
