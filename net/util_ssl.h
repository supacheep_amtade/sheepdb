/*
 * util_ssl.hpp
 *
 *  Created on: Feb 16, 2015
 *      Author: ppus
 */

#ifndef UTIL_SSL_HPP_
#define UTIL_SSL_HPP_

#ifdef __cplusplus
extern "C"
{
#endif

#include <openssl/ssl.h>	/* for SSL utilize */
#include <openssl/err.h>	/* for SSL error */

#define FAIL -1				/* fail const */
#define PORT_SSL 26098		/* port for ssl connection */

/* Structure of arguments to pass to client thread */
typedef struct SSLThreadArgs
{
	SSL *ssl_t;		/* ssl connection descriptor for client */
} SSLThreadArgs;

int OpenListener(int port);		/* create server socket */
SSL_CTX* InitServerCTX(void);	/* initialize SSL server  and create context */
void LoadCertificates(SSL_CTX* ctx, char* CertFile, char* KeyFile);	/* loaf cert. from file */
void ShowCerts(SSL* ssl);		/* show cert */
int OpenConnection(const char *hostname, int port);	/* create socket and connect to server */
SSL_CTX* InitCTX(void);		/* initialize the SSL engine. */

#ifdef __cplusplus
}
#endif

#endif /* UTIL_SSL_HPP_ */
