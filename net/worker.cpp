/*
 * worker.cpp
 *
 *  Created on: Feb 16, 2015
 *      Author: ppus
 */

#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/socket.h>	/* for recv(), send() */
#include <unistd.h>		/* for close() */

#include "worker.hpp"
#include "util_net.h"
#include "../utils/alldef.hpp"
#include "../utils/code_generater.hpp"

CodeGenerater cg;			/* class CodeGenerater */
bool debug;

void WorkerProgram(bool dbg){
	debug = dbg;
	int servSock;                    /* Socket descriptor for server */
	int clntSock;                    /* Socket descriptor for client */
	unsigned short echoServPort;     /* Server port */
	struct ThreadArgs *threadArgs;   /* Pointer to argument structure for thread */

	echoServPort = PORT_WORKER;

	servSock = CreateTCPServerSocket(echoServPort);

	for (;;) /* run forever */{
		pthread_t threadID[2];              /* Thread ID from pthread_create() */
		for (int i = 0; i < 2; i++){
			clntSock = AcceptTCPConnection(servSock);

			/* Create separate memory for client argument */
			if ((threadArgs = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs)))
				   == NULL)
				DieWithError("malloc() failed");
			threadArgs -> clntSock = clntSock;

			/* Create client thread */
			if (pthread_create(&threadID[i], NULL, ThreadWorker, (void *) threadArgs) != 0)
				DieWithError("pthread_create() failed");
//			printf("with thread[%d] %ld\n", (long int) threadID[i], i);
		}

		for (int i = 0; i < 2; i++){
			pthread_detach(threadID[i]);
			pthread_join(threadID[i], NULL);
		}
	}
	pthread_exit(NULL);
	/* NOT REACHED */
}

void *ThreadWorker(void* arg){
	int clntSock;                   /* Socket descriptor for client connection */

	/* Extract socket file descriptor from argument */
	clntSock = ((struct ThreadArgs *) arg) -> clntSock;
	free(arg);              /* Deallocate memory for argument */

	WorkerProcess(clntSock);

	/* Guarantees that thread resources are deallocated upon return */
	pthread_detach(pthread_self());
}

void WorkerProcess(int clntSock){
	char *echoBuffer = (char *)malloc(sizeof(char) * RCVBUFSIZE + 1);	/* Buffer for echo string */
	int recvMsgSize;                    /* Size of received message */
	char *cmdBuffer = (char *)malloc(sizeof(char) * CMDBUFSIZE);	/* Buffer for data */
	int totalRcvdSize = 0;				/* total size of received data */

	/* Receive message from client */
	if ((recvMsgSize = recv(clntSock, echoBuffer, RCVBUFSIZE, 0)) < 0)
		DieWithError("recv() failed");

	strcat(cmdBuffer, echoBuffer);
	totalRcvdSize += recvMsgSize;

	/* Send received string and receive again until end of transmission */
	while (recvMsgSize >= RCVBUFSIZE && echoBuffer[recvMsgSize - 1] != '\n'){      /* zero indicates end of transmission */
		/* See if there is more data to receive */
		if ((recvMsgSize = recv(clntSock, echoBuffer, RCVBUFSIZE, 0)) < 0)
			DieWithError("recv() failed");
		if (recvMsgSize > 0){
			echoBuffer[recvMsgSize] = '\0';
			printf("%s\n", echoBuffer);    /* Print a final linefeed */
			strcat(cmdBuffer, echoBuffer);
 			totalRcvdSize += strlen(echoBuffer);
		}
	}
	
	cmdBuffer[totalRcvdSize + 1] = '\0';	/* close cmdbuffer string */
	
	if (debug)
		printf("%s <- this is the recvd command\n",cmdBuffer);
	char* answer;
	if (strcmp(cmdBuffer, "\n") != 0) { answer = cg.execute(cmdBuffer);} /* execute query */

	if (strcmp(answer, "") == 0){
		answer = "nack";
	}

	if (debug)
		printf("%s <- this is the answer\n",answer);

	/* Answer message back to server */
	if (send(clntSock, answer, strlen(answer), 0) != strlen(answer))
		DieWithError("send() failed");

	/* free heap memmory */
	free(echoBuffer);
//	free(cmdBuffer);	/* error when free */


	close(clntSock);    /* Close client socket */
}
