/*
 * sheepwd.cpp
 *
 *  Created on: Mar 2, 2015
 *      Author: ppus
 */
#include "stdio.h"
#include "../net/worker.hpp"
#include "../utils/demon_utils.hpp"	/* for read_config() and get_ip() */

int main(int argc, char* argv[]){
	char* filename = "conf/sheep_config.json";
	bool isWorker = false;
	bool debug = false;
	read_config(filename);

	if (checkWorker()){
//		/* IMPLEMENT WORKER PROGRAM */
		if (argv[1]){
			printf("Debug mode\n");
			debug = true;
		}
		WorkerProgram(debug);
	}

	return 1;
}
