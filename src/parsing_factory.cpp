/*
 * parsing_factory.cpp
 *
 *  Created on: Mar 4, 2015
 *      Author: ppus
 */

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <fstream>
#include <sys/time.h>
#include <time.h>

//#include "../utils/xml2json.hpp"
#include "../utils/dblp_parser.hpp"

using namespace std;

#include "stdio.h"
#include "stdlib.h"


int main(int argv, char* argc[]){
//	string xml_str, json_str;
//	ifstream inf; ofstream outf;
//	ostringstream oss;
//
//    inf.open(argc[1]);
////	inf.open("/home/ppus/data/dblp-0111.xml");
//    oss.clear();
//	oss << inf.rdbuf();
//	xml_str = oss.str();
//	inf.close();
//	json_str = xml2json(xml_str.c_str());
//
//	outf << json_str;
//	outf.close();
//
//	DBLPParser dblpparser;
//	dblpparser.dojson(json_str.c_str());

	/* timer */
	clock_t before = clock();
	int msec = 0;

	DBLPParser dblpparser;
	dblpparser.dbauthor = (char *)malloc(sizeof(char)*20);
	if (argv == 2 || argv == 3){

		if (argv == 3){
			strcpy(dblpparser.dbauthor,"db/author");
			strcat(dblpparser.dbauthor,argc[2]);
		}else{
			strcpy(dblpparser.dbauthor,"db/author\0");
		}
		printf("db = %s %s\n", dblpparser.dbauthor, argc[1]);
		dblpparser.dofile(argc[1]);
	}else if (argv == 4 || argv == 5){
		char filename[30];
		char numbuf[5];
		char *prefix = "dblp-\0";
		char *subfix = ".xml.js\0";
		if (atoi(argc[3]) >= atoi(argc[2])){
			for (int i = atoi(argc[2]); i <= atoi(argc[3]); i++){
				filename[0] = '\0';
				char *addzero = (char *)malloc(sizeof(char)*4);
				strcat(filename, argc[1]);
				strcat(filename, prefix);
				if (i >= 0 && i < 10){
					addzero = "000";
				}else if (i >= 10 && i < 100){
					addzero = "00";
				}else if (i >= 100 && i < 1000){
					addzero = "0";
				}else{
					addzero = "";
				}
				sprintf(numbuf, "%d", i);
				strcat(filename, addzero);
				strcat(filename, numbuf);
				strcat(filename, subfix);

				if (argv == 5){
					strcpy(dblpparser.dbauthor,"db/author");
					strcat(dblpparser.dbauthor,argc[4]);
				}else{
					strcpy(dblpparser.dbauthor,"db/author\0");
				}
				printf("db = %s %s\n", dblpparser.dbauthor, filename);
				dblpparser.dofile(filename);
			}
		}else{
			printf("arg[2] must be greater than arg[1]\n");
		}
	}

	clock_t difference = clock() - before;
	msec = difference * 1000 / CLOCKS_PER_SEC;
	printf("Time taken %d seconds %d milliseconds\n",
			msec/1000, msec%1000);

	return 0;
}
